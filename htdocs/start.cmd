@echo off
del /s /q cams\*

:start

tskill /A ffmpeg
setlocal EnableDelayedExpansion

for /L %%i IN (1,1,68) DO (
  set cnt=000000%%i
  set cam=!cnt:~-2!
  Echo Load cam#!cam!
  start /low /MIN "" ffmpeg -rtsp_transport tcp -i rtsp://admin:admin@192.168.5.%%i:554/1/h264minor -f image2 -update 1 -s 240x120 -y -r 0.5 -t 1 cams/cam!cam!.png
)
endlocal
start /low /MIN /WAIT "" ffmpeg -rtsp_transport tcp -i rtsp://admin:admin@192.168.5.69:554/1/h264minor -f image2 -update 1 -s 240x120 -y -r 0.5 -t 1 cams/cam69.png
Echo pause..
ping 8.8.8.8 -n 5 > nul
cls
goto start
