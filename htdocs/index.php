<html>

<head>
    <title>GAZPROM CAMS</title>
    <style>
    body {
        overflow: auto;
        background-color: black;
        color: white;
    }

    .cam {
        width: 240px;
        height: 120px;
    }

    #fs {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: auto;
        background: #005500;
        z-index: 9999;
    }
    </style>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
</head>

<body style="padding:0; margin:0; border:none;">
    <img id="fs" onclick="hideCam()" />
    <div style="display: flex; flex-wrap: wrap">
        <?
    require("./cams-list.php");
      for ($index=1; $index<=sizeof($cams); $index++) {
          $ip = isset($cams[$index]) ? $cams[$index] : $index;
          $ip = +$ip < 10 ? '0' . $ip : $ip;
    ?>
        <div style="position:relative">
            <img class='cam' sr='cams/cam<?= $ip ?>.png?' src='cams/cam<?= $ip ?>.png?' onClick='showCam(<?= $ip ?>)' />
            <div
                style="display: none; position:absolute; right:0; top:0; background-color:#000; padding: 2px; border-radius: 0 0 0 10px">
                <?= $index ?>=<?= $ip ?>
            </div>
        </div>
        <?
      }
    ?>
    </div>
    <script>
    let fs = $('#fs');

    function reload() {
        console.log('reload');
        $('.cam').each(function() {
            var src = $(this).attr('sr');
            var d = new Date();
            $(this).attr('src', src + d.getTime());
        })
    };
    setInterval(reload, 2000);


    function showCam(index) {
        fs.show();
        fs.attr('src', 'online.php?cam=' + index);
    }

    function hideCam() {
        fs.hide()
        fs.attr('src', 'online.php');
    }
    </script>
</body>

</html>